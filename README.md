﻿# Movie Characters API
A datastore and interface to store and manipulate movie characters

## Table of contents
- [Background](#background)
- [Getting started](#getting-started) 
- [Maintainers](#maintainers)
- [License](#licence)

## Background
Application constructed in ASP.NET Core and comprise of a database made in SQL Server through EF Core with a RESTful API to allow users to manipulate the data

## Getting started
Clone the repository locally
```
git clone https://gitlab.com/rula3000/movie-characters-api.git
```
Open in Visual Studio.

Assuming you have SQL Server installed: run migrations using Package Manager console. Remember to change the connection string in appsettings.json
```
Update-Database
```
This will create a database in LocalDb, used for development.

## Maintainers
Rune Lauvland (@rula3000)
