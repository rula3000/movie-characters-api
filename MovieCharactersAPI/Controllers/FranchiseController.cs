﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Data;
using MovieCharactersAPI.DTOs.Character;
using MovieCharactersAPI.DTOs.Franchise;
using MovieCharactersAPI.DTOs.Movie;
using MovieCharactersAPI.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class FranchiseController : ControllerBase
    {
        private readonly MovieCharactersDbContext _context;
        private readonly IMapper _mapper;

        public FranchiseController(MovieCharactersDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all franchises
        /// </summary>
        /// <returns>List of franchises</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            var franchises = await _context.Franchises.Include(c => c.Movies).ToListAsync();

            var franchiseRead = _mapper.Map <List<FranchiseReadDTO>>(franchises);

            return Ok(franchiseRead);
        }

        /// <summary>
        /// Gets a single franchise using id
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <returns>Franchise data or not found</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchiseById(int id)
        {
            var franchise = await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f=> f.Id == id);
            if (franchise == null)
            {
                return NotFound();
            }
            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Adds a new franchise
        /// </summary>
        /// <param name="franchise">The franchise data to add</param>
        /// <returns>A franchise ID, franchise data and a responsetype</returns>
        [HttpPost]
        public async Task<ActionResult<FranchiseCreateDTO>> PostFranchise([FromBody] FranchiseCreateDTO franchise)
        {
            var domainFranchise = _mapper.Map<Franchise>(franchise);

            try
            {
                await _context.Franchises.AddAsync(domainFranchise);
                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            var newFranchise = _mapper.Map<FranchiseReadDTO>(domainFranchise);

            return CreatedAtAction("GetFranchiseById", new { Id = newFranchise.Id }, newFranchise);
        }

        /// <summary>
        /// Deletes franchise
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <returns>No Content, bad request or not found</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteFranchise(int id) 
            {
                var domainFranchise = await _context.Franchises.FindAsync(id);

                if (domainFranchise == null)
                {
                    return NotFound();
                }

            try
            {
                _context.Franchises.Remove(domainFranchise);
                await _context.SaveChangesAsync(true);
            }
            catch (Exception e)
            {
                return BadRequest(e.GetType());
            }

            return NoContent();            
        }

        /// <summary>
        /// Updates a franchise
        /// </summary>
        /// <param name="id">Id of the franchise which needs to be updated</param>
        /// <param name="updatedFranchise"></param>
        /// <returns>Success, Bad Request or Not Found</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> PutFranchise(int id, [FromBody] FranchiseUpdateDTO updatedFranchise)
        {
            if (id != updatedFranchise.Id)
            {
                return BadRequest();
            }

            if (!FranchiseExists(id))
            {
                return NotFound();
            }

            var domainFranchise = _mapper.Map<Franchise>(updatedFranchise);
            _context.Entry(domainFranchise).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Updates movies in a franchise
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <param name="movieIds">Array of movie id's</param>
        /// <returns>Success or Not Found</returns>
        [HttpPut("{id}/moviesinfranchise")]       
        public async Task<ActionResult> UpdateMoviesInFranchise(int id, [FromBody] List<int> movieIds)
        {
            var franchise = _context.Franchises.Include(f => f.Movies).FirstOrDefault(f => f.Id == id);

            if (franchise == null)
            {
                return NotFound();
            }
                        
            franchise.Movies = new List<Movie>();
            
            foreach (var movieId in movieIds)
            {
                var movie = await _context.Movies.FindAsync(movieId);

                if (movie == null)
                {
                    return NotFound();
                }

                franchise.Movies.Add(movie);
            }
            
            _context.SaveChanges();
            
            return NoContent();
        }

        /// <summary>
        /// Gets all the movies in a franchise
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <returns>List of movies or Not Found</returns>
        [HttpGet("{id}/moviesinfranchise")]
        public async Task<ActionResult> GetMoviesInFranchise(int id)
        {
            var franchise = await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);

            if (franchise == null)
            {
                return NotFound();
            }

            var movieRead = _mapper.Map<List<MovieReadDTO>>(franchise.Movies);

            return Ok(movieRead);
        }

        /// <summary>
        /// Gets all the characters in a franchise
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <returns>List of characters or Not Found</returns>
        [HttpGet("{id}/charactersinfranchise")]
        public async Task<ActionResult> GetCharactersInFranchise(int id)
        {
            var franchise = await _context.Franchises.Include(f => f.Movies).ThenInclude(m => m.Characters).FirstOrDefaultAsync(f => f.Id == id);

            if (franchise == null)
            {
                return NotFound();
            }

            var movies = franchise.Movies;

            var characters = new List<Character>();
            foreach (Movie movie in movies)
            {
                if (movie.Characters != null)
                {
                    characters.AddRange(movie.Characters);
                }
                
            }

            var characterRead = _mapper.Map<List<CharacterReadDTO>>(characters.Distinct()).OrderBy(c => c.Id);

            return Ok(characterRead);
        }

        /// <summary>
        /// Checks if a franchise exists.
        /// </summary>
        /// <param name="id">Franchise ID.</param>
        /// <returns>>True or false if exists in the database</returns>
        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
