﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Data;
using MovieCharactersAPI.DTOs.Character;
using MovieCharactersAPI.DTOs.Franchise;
using MovieCharactersAPI.DTOs.Movie;
using MovieCharactersAPI.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class MovieController : ControllerBase
    {
        private readonly MovieCharactersDbContext _context;
        private readonly IMapper _mapper;

        public MovieController(MovieCharactersDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all movies
        /// </summary>
        /// <returns>List of movies</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            var domainMovies = await _context.Movies.Include(m => m.Characters).ToListAsync();

            var movies = _mapper.Map<List<MovieReadDTO>>(domainMovies);

            return movies;
        }

        /// <summary>
        /// Gets a single movie using id
        /// </summary>
        /// <param name="id">Id of the movie</param>
        /// <returns>Movie data or Not Found</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovieById(int id)
        {
            var movie = await _context.Movies.Include(m => m.Characters).FirstOrDefaultAsync(m => m.Id == id);
            if (movie == null)
            {
                return NotFound();
            }
            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Adds a new movie
        /// </summary>
        /// <param name="movie">The movie data to add</param>
        /// <returns>>A movie ID, movie data and a responsetype</returns>
        [HttpPost]
        public async Task<ActionResult<MovieCreateDTO>> PostMovie([FromBody] MovieCreateDTO movie)
        {
            var domainMovie = _mapper.Map<Movie>(movie);

            try
            {
                await _context.Movies.AddAsync(domainMovie);
                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            var newMovie = _mapper.Map<MovieReadDTO>(domainMovie);

            return CreatedAtAction("GetMovieById", new { Id = newMovie.Id }, newMovie);
        }

        /// <summary>
        /// Deletes movie
        /// </summary>
        /// <param name="id">Movie id</param>
        /// <returns>No Content or Not Found</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteMovie(int id)
        {
            var domainMovie = await _context.Movies.FindAsync(id);

            if (domainMovie == null)
            {
                return NotFound();
            }

            try
            {
                _context.Movies.Remove(domainMovie);
                await _context.SaveChangesAsync(true);
            }
            catch (Exception e)
            {
                return BadRequest(e.GetType());
            }

            return NoContent();
        }

        /// <summary>
        /// Updates a movie
        /// </summary>
        /// <param name="id">Id of the movie which needs to be updated</param>
        /// <param name="updatedMovie"></param>
        /// <returns>Success, Bad Request or Not Found</returns>
        [HttpPut]
        public async Task<ActionResult> PutMovie(int id, [FromBody] MovieUpdateDTO updatedMovie)
        {
            if (id != updatedMovie.Id)
            {
                return BadRequest();
            }

            if (!MovieExists(id))
            {
                return NotFound();
            }

            Movie domainMovie = _mapper.Map<Movie>(updatedMovie);
            _context.Entry(domainMovie).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Updates characters in a movie
        /// </summary>
        /// <param name="id">Movie id</param>
        /// <param name="characterIds">Array of character id's</param>
        /// <returns>Success or Not Found</returns>
        [HttpPut("{id}/charactersinmovie")]
        public async Task<ActionResult> UpdateCharactersInMovie(int id, [FromBody] List<int> characterIds)
        {
            var movie = await _context.Movies.Include(m => m.Characters).FirstOrDefaultAsync(m => m.Id == id);

            if (movie == null)
            {
                return NotFound();
            }

            movie.Characters = new List<Character>();

            foreach (var characterId in characterIds)
            {
                var character = await _context.Characters.FindAsync(characterId);

                if (character == null)
                {
                    return NotFound();
                }

                movie.Characters.Add(character);
            }

            _context.SaveChanges();

            return NoContent();
        }

        /// <summary>
        /// Gets all the characters in a movie
        /// </summary>
        /// <param name="id">Movie id</param>
        /// <returns>List of characters or Not Found</returns>
        [HttpGet("{id}/charactersinmovie")]
        public async Task<ActionResult> GetCharactersInMovie(int id)
        {
            var movie = await _context.Movies.Include(m => m.Characters).ThenInclude(c => c.Movies).FirstOrDefaultAsync(m => m.Id == id);

            if (movie == null)
            {
                return NotFound();
            }

            var characterRead = _mapper.Map<List<CharacterReadDTO>>(movie.Characters);

            return Ok(characterRead);

        }

        /// <summary>
        /// Checks if a movie exists.
        /// </summary>
        /// <param name="id">Movie ID.</param>
        /// <returns>True or false if exists in the database</returns>
        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
    }
}
