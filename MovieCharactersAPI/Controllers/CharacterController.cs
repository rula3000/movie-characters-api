﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Data;
using MovieCharactersAPI.DTOs.Character;
using MovieCharactersAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class CharacterController : ControllerBase
    {
        private readonly MovieCharactersDbContext _context;
        private readonly IMapper _mapper;
        public CharacterController(MovieCharactersDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all characters
        /// </summary>
        /// <returns>List of characters</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            var characters = await _context.Characters.Include(c => c.Movies).ToListAsync();

            var characterRead = _mapper.Map<List<CharacterReadDTO>>(characters);

            return Ok(characterRead);
        }

        /// <summary>
        /// Gets a single character using id
        /// </summary>
        /// <param name="id">Id of the character</param>
        /// <returns>Character data or not found</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacterById(int id)
        {
            var character = await _context.Characters.Include(c => c.Movies).FirstOrDefaultAsync(c => c.Id == id);
            if (character == null)
            {
                return NotFound();
            }
            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Adds a new character
        /// </summary>
        /// <param name="character">The character data to add</param>
        /// <returns>A character ID, character data and a responsetype</returns>
        [HttpPost]
        public async Task<ActionResult<CharacterCreateDTO>> PostCharacter([FromBody] CharacterCreateDTO character)
        {
            var domainCharacter = _mapper.Map<Character>(character);

            try
            {
                await _context.Characters.AddAsync(domainCharacter);
                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            var newCharacter = _mapper.Map<CharacterReadDTO>(domainCharacter);

            return CreatedAtAction("GetCharacterById", new { Id = newCharacter.Id }, newCharacter);
        }

        /// <summary>
        /// Deletes character
        /// </summary>
        /// <param name="id">Character id</param>
        /// <returns>No content or not found</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteCharacter(int id)
        {
            var domainCharacter = await _context.Characters.FindAsync(id);
            if (domainCharacter == null)
            {
                return NotFound();
            }

            try
            {
                _context.Characters.Remove(domainCharacter);
                await _context.SaveChangesAsync(true);
            }
            catch (Exception e)
            {
                return BadRequest(e.GetType());
            }

            return NoContent();
        }

        /// <summary>
        /// Updates a character
        /// </summary>
        /// <param name="id">Id of the character which needs to be updated</param>
        /// <param name="updatedCharacter">The character object which will replace the character with Id</param>
        /// <returns>Success, Bad Request or Not Found</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> PutCharacter(int id, [FromBody] CharacterUpdateDTO updatedCharacter)
        {
            if (id != updatedCharacter.Id)
            {
                return BadRequest();
            }

            if (!CharacterExists(id))
            {
                return NotFound();
            }

            var domainCharacter = _mapper.Map<Character>(updatedCharacter);
            _context.Entry(domainCharacter).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Checks if a character exists.
        /// </summary>
        /// <param name="id">Character ID.</param>
        /// <returns>>True or false if exists in the database</returns>
        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
    }
}
