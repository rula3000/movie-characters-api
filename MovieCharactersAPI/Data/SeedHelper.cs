﻿using MovieCharactersAPI.Models;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Data
{
    public class SeedHelper
    {
        /// <summary>
        /// Helper method for seeding franchises 
        /// </summary>
        /// <returns>List of Franchises</returns>
        public static IEnumerable<Franchise> GetFranchiseSeeds()
        {
            IEnumerable<Franchise> seedFranchise = new List<Franchise>()
            {
                new Franchise
                {
                    Id = 1,
                    Name = "Marvel Cinematic Universe",
                    Description = "The Marvel Cinematic Universe (MCU) films are a series of American superhero films produced by Marvel Studios based on characters that appear in publications by Marvel Comics."
                },

                new Franchise
                {
                    Id = 2,
                    Name = "Lord of the Rings",
                    Description = "The Lord of the Rings is a series of three epic fantasy adventure films directed by Peter Jackson, based on the novel The Lord of the Rings by J. R. R. Tolkien."
                }
            };
            return seedFranchise;
        }

        /// <summary>
        /// Helper method for seeding movies
        /// </summary>
        /// <returns>List of Movies</returns>
        public static IEnumerable<Movie> GetMovieSeeds()
        {
            IEnumerable<Movie> seedMovie = new List<Movie>()
            {
                new Movie
                {
                    Id = 1,
                    Title = "Iron Man",
                    Genre = "Sci-Fi",
                    ReleaseYear = "2008",
                    Director = "Jon Favreau",
                    Picture = "https://www.imdb.com/title/tt0371746/mediaviewer/rm1544850432/?ref_=tt_ov_i",
                    Trailer = "https://www.imdb.com/video/vi447873305/?playlistId=tt0371746&ref_=tt_pr_ov_vi",
                    FranchiseId = 1
                },

                new Movie
                {
                    Id = 2,
                    Title = "The Incredible Hulk",
                    Genre = "Sci-Fi",
                    ReleaseYear = "2008",
                    Director = "Louis Leterrier",
                    Picture = "https://www.imdb.com/title/tt0800080/mediaviewer/rm2081134080/?ref_=tt_ov_i",
                    Trailer = "https://www.imdb.com/video/vi1726152985/?playlistId=tt0800080&ref_=tt_ov_vi",
                    FranchiseId = 1
                },

                new Movie
                {
                    Id = 3,
                    Title = "The Fellowship of the Ring",
                    Genre = "Fantasy",
                    ReleaseYear = "2001",
                    Director = "Peter Jackson",
                    Picture = "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976/?ref_=tt_ov_i",
                    Trailer = "https://www.imdb.com/video/vi684573465/?playlistId=tt0120737&ref_=tt_ov_vi",
                    FranchiseId = 2
                },

                 new Movie
                {
                    Id = 4,
                    Title = "The Two Towers",
                    Genre = "Fantasy",
                    ReleaseYear = "2002",
                    Director = "Peter Jackson",
                    Picture = "https://www.imdb.com/title/tt0167261/mediaviewer/rm306845440/?ref_=tt_ov_i",
                    Trailer = "https://www.imdb.com/video/vi701350681/?playlistId=tt0167261&ref_=tt_ov_vi",
                    FranchiseId = 2
                }

            };
            return seedMovie;
        }

        /// <summary>
        /// Helper method for seeding characters
        /// </summary>
        /// <returns>List of Characters</returns>
        public static IEnumerable<Character> GetCharacterSeeds()
        {
            IEnumerable<Character> seedCharacter = new List<Character>()
            {
                //Iron Man
                new Character
                {
                    Id = 1,
                    Name = "Tony Stark",
                    Alias = "",
                    Gender = "Male",
                    Picture = "https://m.media-amazon.com/images/M/MV5BMTI5ODY5NTUzMF5BMl5BanBnXkFtZTcwOTAzNTIzMw@@._V1_SY100_CR21,0,100,100_AL_.jpg"
                },

                new Character
                {

                    Id = 2,
                    Name = "Pepper Potts",
                    Alias = "",
                    Gender = "Female",
                    Picture = "https://m.media-amazon.com/images/M/MV5BMTM0MDczMjkwNV5BMl5BanBnXkFtZTcwNTUyNTIzMw@@._V1_SX100_CR0,0,100,100_AL_.jpg"
                },


                //The Incredible Hulk
                new Character
                {

                    Id = 3,
                    Name = "Bruce Banner",
                    Alias = "",
                    Gender = "Male",
                    Picture = "https://m.media-amazon.com/images/M/MV5BMTQ4MTY4OTUzOV5BMl5BanBnXkFtZTcwNzk3NTAyNw@@._V1_SY100_CR25,0,100,100_AL_.jpg"
                },

                //The Fellowship of the Ring, The Two Towers
                new Character
                {

                    Id = 4,
                    Name = "Frodo",
                    Alias = "",
                    Gender = "Male",
                    Picture = "https://m.media-amazon.com/images/M/MV5BMTYxNjY1ODQ3MF5BMl5BanBnXkFtZTcwMjgwMjk2Mw@@._V1_SY100_CR24,0,100,100_AL_.jpg"
                }
                

            };
            return seedCharacter;
        }
    }
}
