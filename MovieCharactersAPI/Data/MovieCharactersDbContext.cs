﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using System.Security.Cryptography.X509Certificates;

namespace MovieCharactersAPI.Data
{
    public class MovieCharactersDbContext : DbContext
    {
        public MovieCharactersDbContext(DbContextOptions options): base(options)
        {
            
        }

        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchise>().HasData(SeedHelper.GetFranchiseSeeds());
            modelBuilder.Entity<Movie>().HasData(SeedHelper.GetMovieSeeds());
            modelBuilder.Entity<Character>().HasData(SeedHelper.GetCharacterSeeds());
            modelBuilder.Entity("CharacterMovie").HasData(
                new { CharactersId = 1, MoviesId = 1 },
                new { CharactersId = 2, MoviesId = 1 },
                new { CharactersId = 3, MoviesId = 2 },
                new { CharactersId = 4, MoviesId = 3 },
                new { CharactersId = 4, MoviesId = 4 });
        }
    }
}
