﻿using MovieCharactersAPI.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.DTOs.Character
{
    public class CharacterReadDTO
    {
        public int Id { get; set; }
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        [MaxLength(100)]
        public string Alias { get; set; }
        [MaxLength(100)]
        public string Gender { get; set; }
        [MaxLength(500)]
        public string Picture { get; set; }

        // many to many
        public List<int> Movies { get; set; }
    }
}
