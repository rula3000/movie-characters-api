﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.DTOs.Character
{
    public class CharacterCreateDTO
    {
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        [MaxLength(100)]
        public string Alias { get; set; }
        [MaxLength(100)]
        public string Gender { get; set; }
        [MaxLength(500)]
        public string Picture { get; set; }
        public object Movies { get; set; }
    }
}
