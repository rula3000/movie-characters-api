﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.DTOs.Franchise
{
    public class FranchiseCreateDTO
    {
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
    }
}
