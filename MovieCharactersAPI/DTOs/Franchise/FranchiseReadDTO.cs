﻿using MovieCharactersAPI.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.DTOs.Franchise
{
    public class FranchiseReadDTO
    {
        public int Id { get; set; }
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }

        // many to one
        public List<int> Movies { get; set; }
    }
}
