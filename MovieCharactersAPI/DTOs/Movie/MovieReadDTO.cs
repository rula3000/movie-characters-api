﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.DTOs.Movie
{
    public class MovieReadDTO
    {
        public int Id { get; set; }
        [MaxLength(100)]
        [Required]
        public string Title { get; set; }
        [MaxLength(100)]
        public string Genre { get; set; }
        [MaxLength(100)]
        public string ReleaseYear { get; set; }
        [MaxLength(100)]
        public string Director { get; set; }
        [MaxLength(500)]
        public string Picture { get; set; }
        [MaxLength(500)]
        public string Trailer { get; set; }

        // one to many
        public int? FranchiseId { get; set; }

        // many to many
        public List<int> Characters { get; set; }
    }
}
