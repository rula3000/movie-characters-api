﻿using AutoMapper;
using MovieCharactersAPI.DTOs.Character;
using MovieCharactersAPI.Models;
using System.Linq;

namespace MovieCharactersAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<CharacterCreateDTO, Character>();
            CreateMap<CharacterUpdateDTO, Character>();
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(crdto => crdto.Movies,
                    opt => opt.MapFrom(c => c.Movies
                        .Select(m => m.Id).ToArray()));
        }       
    }
}
