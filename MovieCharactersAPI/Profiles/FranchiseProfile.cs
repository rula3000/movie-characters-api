﻿using AutoMapper;
using MovieCharactersAPI.DTOs.Franchise;
using MovieCharactersAPI.Models;
using System.Linq;

namespace MovieCharactersAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<FranchiseCreateDTO, Franchise>();
            CreateMap<FranchiseUpdateDTO, Franchise>();
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(frdto => frdto.Movies,
                    opt => opt.MapFrom(f => f.Movies
                        .Select(m => m.Id).ToArray()));
        }
    }
}
