﻿using AutoMapper;
using MovieCharactersAPI.DTOs.Movie;
using MovieCharactersAPI.Models;
using System.Linq;

namespace MovieCharactersAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<MovieCreateDTO, Movie>();
            CreateMap<MovieUpdateDTO, Movie>();
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mrdto => mrdto.Characters,
                    opt => opt.MapFrom(m => m.Characters
                        .Select(c => c.Id).ToArray()));
        }       
    }
}
