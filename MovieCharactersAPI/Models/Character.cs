﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace MovieCharactersAPI.Models
{
    public class Character
    {
        public int Id { get; set; }
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        [MaxLength(100)]
        public string Alias { get; set; }
        [MaxLength(100)]
        public string Gender { get; set; }
        [MaxLength(500)]
        public string Picture { get; set; }

        // many to many
        public ICollection<Movie> Movies { get; set; } 

    }
}
