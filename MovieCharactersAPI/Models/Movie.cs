﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models
{
    public class Movie
    {
        public int Id { get; set; }
        [MaxLength(100)]
        [Required]
        public string Title { get; set; }
        [MaxLength(100)]
        public string Genre { get; set; }
        [MaxLength (100)]
        public string ReleaseYear { get; set; }
        [MaxLength(100)]
        public string Director { get; set; }
        [MaxLength(500)]
        public string Picture { get; set; }
        [MaxLength(500)]
        public string Trailer { get; set; }

        // one to many
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }

        // many to many
        public ICollection<Character> Characters { get; set; }
    }
}
