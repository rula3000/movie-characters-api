﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieCharactersAPI.Migrations
{
    public partial class Seeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "Gender", "Name", "Picture" },
                values: new object[,]
                {
                    { 1, "", "Male", "Tony Stark", "https://m.media-amazon.com/images/M/MV5BMTI5ODY5NTUzMF5BMl5BanBnXkFtZTcwOTAzNTIzMw@@._V1_SY100_CR21,0,100,100_AL_.jpg" },
                    { 2, "", "Female", "Pepper Potts", "https://m.media-amazon.com/images/M/MV5BMTM0MDczMjkwNV5BMl5BanBnXkFtZTcwNTUyNTIzMw@@._V1_SX100_CR0,0,100,100_AL_.jpg" },
                    { 3, "", "Male", "Bruce Banner", "https://m.media-amazon.com/images/M/MV5BMTQ4MTY4OTUzOV5BMl5BanBnXkFtZTcwNzk3NTAyNw@@._V1_SY100_CR25,0,100,100_AL_.jpg" },
                    { 4, "", "Male", "Frodo", "https://m.media-amazon.com/images/M/MV5BMTYxNjY1ODQ3MF5BMl5BanBnXkFtZTcwMjgwMjk2Mw@@._V1_SY100_CR24,0,100,100_AL_.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "The Marvel Cinematic Universe (MCU) films are a series of American superhero films produced by Marvel Studios based on characters that appear in publications by Marvel Comics.", "Marvel Cinematic Universe" },
                    { 2, "The Lord of the Rings is a series of three epic fantasy adventure films directed by Peter Jackson, based on the novel The Lord of the Rings by J. R. R. Tolkien.", "Lord of the Rings" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[,]
                {
                    { 1, "Jon Favreau", 1, "Sci-Fi", "https://www.imdb.com/title/tt0371746/mediaviewer/rm1544850432/?ref_=tt_ov_i", "2008", "Iron Man", "https://www.imdb.com/video/vi447873305/?playlistId=tt0371746&ref_=tt_pr_ov_vi" },
                    { 2, "Louis Leterrier", 1, "Sci-Fi", "https://www.imdb.com/title/tt0800080/mediaviewer/rm2081134080/?ref_=tt_ov_i", "2008", "The Incredible Hulk", "https://www.imdb.com/video/vi1726152985/?playlistId=tt0800080&ref_=tt_ov_vi" },
                    { 3, "Peter Jackson", 2, "Fantasy", "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976/?ref_=tt_ov_i", "2001", "The Fellowship of the Ring", "https://www.imdb.com/video/vi684573465/?playlistId=tt0120737&ref_=tt_ov_vi" },
                    { 4, "Peter Jackson", 2, "Fantasy", "https://www.imdb.com/title/tt0167261/mediaviewer/rm306845440/?ref_=tt_ov_i", "2002", "The Two Towers", "https://www.imdb.com/video/vi701350681/?playlistId=tt0167261&ref_=tt_ov_vi" }
                });

            migrationBuilder.InsertData(
                table: "CharacterMovie",
                columns: new[] { "CharactersId", "MoviesId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 3, 2 },
                    { 4, 3 },
                    { 4, 4 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 2, 1 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 3, 2 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 4, 3 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 4, 4 });

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
